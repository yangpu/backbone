/**
 * Created by xiaoxiaosu on 2015/12/14.
 */
//第三次提交
var path = require('path');
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports={
    entry:"./demo/main.js",
    output:{
        path:"./output",
        filename:"bundle.js"//默认输出一个bundle.js文件
    },
    resolve: {
        root: path.resolve('./'),
    },
    devServer:{
        historyApiFallback:true,
        hot:true,
        inline:true,
        progress:true,
        port:9090 //端口你可以自定义
    },
    module:{
        loaders:[
            //这些都是组件化的加载器
            // {test:/\.(js)$/,loader:"jsx-loader?harmony"},
            {test:/\.(css)$/,loader:"style!css!"},
            {test: /\.less/,loader: 'style!css!less!'},
            {test: /\.scss$/,loader: 'style!css!sass!scss!'},
            {test:/\.(png!jpg)$/,loader:"url?size=8192"},
            { test: /\.ejs$/, loader: "ejs-loader"}
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./app/template.html',
            filename:'template.html',//bundle.js文件默认输入这个文件
            inject:'body',
            hash:true,//热更新，后面带一点参数
        }),
        new webpack.HotModuleReplacementPlugin(),//webpack自带的插件
    ]
}