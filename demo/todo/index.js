/**
 * Created by 13258 on 2017/1/25.
 */
var _ = require('underscore');
var Backbone = require('backbone');
var $ = require('jquery');
var Item = require('./item');

var todo = Backbone.View.extend({
    template: require('./todo.ejs'),
    initialize(){
        this.render();//调用渲染整个文件,加载ejx成功了
        this.initEvent();//调用点击事件
    },
    render(){
        this.$el.html(this.template());
        this.$list = this.$el.find('.JS-todo-list');
        this.$input = this.$el.find('.JS-input');
        this.$add = this.$el.find('.JS-add');
        this.itemViews = [];//创建一个空的数组来存放backbone的值
        this.list = new Backbone.Collection();
        this.ids = 0;
    },
    initEvent(){
        var that = this;
        this.$el.on('click', '.JS-add', function () {
            var text = that.$input.val();//点击的时候数据增加,而不是直接增加视图，这样耦合度不好
            that.list.add({
                text: text,
                id: that.ids,
                edit:false

            })
            that.ids++;
            that.trigger('add-todo',text)//手动触发一个事件
        })
        this.list.on('add', function (model) {//数据增加的时候使视图发生改变,传一个model进来
            var item = new Item({
                model: model,
                parent: that,
                list: that.list
            })
            that.itemViews.push(item);//把所有对象存进去
            that.$list.append(item.$el);

        })
        this.list.on('remove', function (model) {//删除的操作
            that.itemViews.filter(function (view) {//view就是数组里面的每一个对象,删除数组的元素
                return view.model.get('id') != model.get('id');
            })
            //console.log(model)
            var view = _.find(that.itemViews,function (view) {//删除dom元素的
                return view.model.get('id') == model.get('id');
            })
            view.destroy();
        })

    }
})

module.exports = todo;


// var json = [
//     {name: 'aaa', id: 1},
//     {name: 'bbb', id: 2},
//     {name: 'ccc', id: 3},
//     {name: 'ddd', id: 4},
// ]
// json = json.filter(function (obj) {
//     console.log(arguments);//第一个参数是obj，第二个参数是下标，第三个参数是整个json数组
//     return obj.id == 2;//返回一个id是2的对象
// })