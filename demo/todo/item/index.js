/**
 * Created by 13258 on 2017/1/26.
 */

var _ = require('underscore');
var Backbone = require('backbone');
var $ = require('jquery');


var Item = Backbone.View.extend({
    template: require('./todo-item.ejs'),//加载整个ejx文件
    initialize(options){//初始化渲染整个html文本
        this.render();
        this.initEvent();//绑定事件
        this.model = options.model;
        this.parent = options.parent;
        this.list = options.list;
    },
    render(){
        var data = this.model.toJSON();
        //console.log(data)
        this.$el.html(this.template(data));//把整个ejs文件放在视图下面,这个没有问题
        this.$remove = this.$el.find('.JS-remove');//取到选择器里面的结构值
        this.$text = this.$el.find('.JS-text');
        this.$input = this.$el.find('.JS-input');

        if (data.edit){
            this.$input.show();
            this.$text.hide();
        }else {
            this.$input.hide();
            this.$text.show();
        }
    },
    initEvent(){
        var that = this;
        this.$el.on('click','.JS-text',function () {
            that.model.set('edit',true)
        })
        this.$el.on('click','.JS-remove',function () {
            // 获取删除的id
            var id = that.model.get('id');
            that.list.remove(id);
            that.parent.trigger('remove-todo',that.model.get('text'))
        })
        this.$el.on('keydown','.JS-input',function (e) {
            if(e.keyCode == 13){//回车键
                var text = that.$input.val();
                that.parent.trigger('edit-todo',that.model.get('text'),text)
                that.model.set({
                   text:text,
                   edit:false
               })//改变数据，不能直接改变，html（）,这样就不是单向的数据流
                //that.render()这样写的话数据耦合不好
            }
        })
        this.model.on('change',function () {//数据发生变化，视图发生变化,重新渲染
            that.render();
        })
    },
    destroy(){//删除元素
        this.$el.remove();
    }
})
module.exports = Item;



// 用户操作------数据变化-------视图变化


