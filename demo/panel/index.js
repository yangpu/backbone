/**
 * Created by 13258 on 2017/1/26.
 */
var _ = require('underscore');
var Backbone = require('backbone');
var $ = require('jquery');

require('./panel.less');//导入less文件


var panel = Backbone.View.extend({
    template: require('./panel.ejs'),//这是一个ejs的模板文件
    initialize(options){

        //this.list = options.list;
        this.render();
    },
    render(){
        //var list = this.list.toJSON();
        this.$el.html(this.template())
        this.$panel = this.$el.find('.panel');//渲染之后才获取
        console.log(this.$panel)

    },
    addMessage(text){
        this.$panel.append('<li>' + text + '</li>');
    }
})
module.exports = panel;