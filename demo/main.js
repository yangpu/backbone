/**
 * Created by 13258 on 2017/1/25.
 */
//第二次提交
//第三次提交
var _ = require('underscore');
var Backbone = require('backbone');
var $ = require('jquery');
var todo = require('./todo');
var panel = require('./panel');

global.$doc = $("#root");

var app = Backbone.View.extend({
    initialize(){
        this.render();
        this.initEvent();//指挥panel做一些事情
    },
    render(){
        this.todo = new todo();//首先实例化一个todo
        $doc.append(this.todo.$el)//这是backbone下面的一个dom元素
        this.panel = new panel();
        $doc.append(this.panel.$el);
    },
    initEvent(){
        var that = this;
        this.todo.on('add-todo', function (text) {
            that.panel.addMessage('你刚刚增加了一项' + text);
        })
        this.todo.on('remove-todo', function (text) {
            that.panel.addMessage('你刚刚删除了一项' + text);
        })
        this.todo.on('edit-todo', function (text1, text2) {
            that.panel.addMessage('你刚刚修改了一项' + text1 + '----' + text2);
        })
    }
})

new app();

